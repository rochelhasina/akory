const { execSync } = require("child_process");

const OPTIONS = {
  timeout: 10000,
  killSignal: "SIGKILL",
};

describe("screenshot", () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });
  it("should take a screenshot", async () => {
    const fileName = `screenshot-${Date.now()}.png`;
    try {
      execSync(
        `xcrun simctl io booted screenshot /akory/ios/fastlane/screenshots/${fileName}`,
        OPTIONS
      );
    } catch (error) {
      console.error(`Error taking screenshot: ${error}`);
    }
  });
});
